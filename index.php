<!doctype html>
<html lang="pt-br">
	<head>
		<!-- Meta tags padrões -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">

		<title>Template Base</title>

		<!-- Meu sass com bootstrap compilado -->
		<link rel="stylesheet" href="css/site.css">
	</head>
<body>

<nav class="navbar navbar-light bg-faded">
	<div class="container">
		<div class="nav navbar-nav">
			<a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
			<a class="nav-item nav-link" href="#">Features</a>
			<a class="nav-item nav-link" href="#">Pricing</a>
			<a class="nav-item nav-link" href="#">About</a>
		</div>
	</div>
</nav>

<div class="container">
	<table class="table table-inverse">
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
</div>

<div class="stickyfooter"></div>	
<footer>
	<nav class="navbar navbar-light bg-faded">
		<div class="container">
			<div class="nav navbar-nav">
				<a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a>
				<a class="nav-item nav-link" href="#">Features</a>
				<a class="nav-item nav-link" href="#">Pricing</a>
				<a class="nav-item nav-link" href="#">About</a>
			</div>
		</div>
	</nav>
</footer>


<!-- jQuery, tether e bootstrap js -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"></script>

<!-- Meus scripts principais -->
<script src="js/jquery-site.js"></script>
<script src="js/javascript-site.js"></script>

</body>
</html>