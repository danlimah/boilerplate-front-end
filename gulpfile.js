var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var watch = require('gulp-watch');

// Task para o Sass
gulp.task('sass', function(){
	return sass('sass/**/*.scss').pipe(gulp.dest('css'))
});

// Task para o Watch
gulp.task('watch', function(){
	gulp.watch('sass/**/*.scss', ['sass'])
});

// Task Padrão
gulp.task('default', ['sass', 'watch']);